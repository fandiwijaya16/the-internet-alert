from addons.web_extensions import WebExtensions
from selenium.webdriver.common.by import By
from src.testproject.classes import DriverStepSettings, StepSettings
from src.testproject.decorator import report_assertion_errors
from src.testproject.enums import SleepTimingType
from src.testproject.enums import TakeScreenshotConditionType, SleepTimingType
from src.testproject.sdk.drivers import webdriver
import pytest


"""
This pytest test was automatically generated by TestProject
    Project: My first Project
    Package: TestProject.Generated.Tests.MyFirstProject
    Test: The Internet (Testing 1)
    Generated by: Fandi Wijaya (fandiwijaya16@gmail.com)
    Generated on 04/18/2022, 06:10:22
"""


@pytest.fixture()
def driver():
    driver = webdriver.Chrome(token="QeZny7buq7t0fmolgNeB0n5Oyo8uJ-lQGoHRO2neLEo",
                              project_name="My first Project",
                              job_name="The Internet (Testing 1)")
    step_settings = StepSettings(timeout=15000,
                                 sleep_time=500,
                                 sleep_timing_type=SleepTimingType.Before)
    with DriverStepSettings(driver, step_settings):
        yield driver
    driver.quit()


@report_assertion_errors
def test_main(driver):
    # Test Parameters
    # Auto generated application URL parameter
    ApplicationURL = "https://the-internet.herokuapp.com/javascript_alerts"

    # 1. Navigate to '{ApplicationURL}'
    # Navigates the specified URL (Auto-generated)
    step_settings = StepSettings(timeout=25000,
                                 sleep_time=1000,
                                 sleep_timing_type=SleepTimingType.Before,
                                 screenshot_condition=TakeScreenshotConditionType.Always)
    with DriverStepSettings(driver, step_settings):
        driver.get(f'{ApplicationURL}')

    # 2. Click 'Click for JS Alert'
    click_for_js_alert = driver.find_element(By.XPATH,
                                             "//button[. = 'Click for JS Alert']")
    click_for_js_alert.click()

    # 3. Accept alert
    driver.addons().execute(
        WebExtensions.acceptalert(
            pause=0))

    # 4. Does 'You successfully clicked an alert' contain 'You successfully clicked an alert'?
    step_settings = StepSettings(timeout=25000,
                                 sleep_time=1000,
                                 sleep_timing_type=SleepTimingType.Before,
                                 screenshot_condition=TakeScreenshotConditionType.Always)
    with DriverStepSettings(driver, step_settings):
        you_successfully_clicked_an_alert = driver.find_element(By.CSS_SELECTOR,
                                                                "#result")
        step_output = you_successfully_clicked_an_alert.text
        assert step_output and (
            "You successfully clicked an alert" in step_output)

    # 5. Click 'Click for JS Confirm'
    click_for_js_confirm = driver.find_element(By.XPATH,
                                               "//button[. = 'Click for JS Confirm']")
    click_for_js_confirm.click()

    # 6. Accept alert
    driver.addons().execute(
        WebExtensions.acceptalert(
            pause=0))

    # 7. Does 'You successfully clicked an alert' contain 'You clicked: Ok'?
    step_settings = StepSettings(timeout=25000,
                                 sleep_time=1000,
                                 sleep_timing_type=SleepTimingType.Before,
                                 screenshot_condition=TakeScreenshotConditionType.Always)
    with DriverStepSettings(driver, step_settings):
        you_successfully_clicked_an_alert = driver.find_element(By.CSS_SELECTOR,
                                                                "#result")
        step_output = you_successfully_clicked_an_alert.text
        assert step_output and ("You clicked: Ok" in step_output)

    # 8. Click 'Click for JS Confirm'
    click_for_js_confirm = driver.find_element(By.XPATH,
                                               "//button[. = 'Click for JS Confirm']")
    click_for_js_confirm.click()

    # 9. Dismiss alert
    driver.addons().execute(
        WebExtensions.dismissalert(
            pause=0))

    # 10. Does 'You successfully clicked an alert' contain 'You clicked: Cancel'?
    step_settings = StepSettings(timeout=25000,
                                 sleep_time=1000,
                                 sleep_timing_type=SleepTimingType.Before,
                                 screenshot_condition=TakeScreenshotConditionType.Always)
    with DriverStepSettings(driver, step_settings):
        you_successfully_clicked_an_alert = driver.find_element(By.CSS_SELECTOR,
                                                                "#result")
        step_output = you_successfully_clicked_an_alert.text
        assert step_output and ("You clicked: Cancel" in step_output)

    # 11. Click 'Click for JS Prompt'
    click_for_js_prompt = driver.find_element(By.XPATH,
                                              "//button[. = 'Click for JS Prompt']")
    click_for_js_prompt.click()

    # 12. Send 'Test Alert' to alert
    driver.addons().execute(
        WebExtensions.sendkeystoalert(
            keys="Test Alert"))

    # 13. Accept alert
    driver.addons().execute(
        WebExtensions.acceptalert(
            pause=0))

    # 14. Does 'You successfully clicked an alert' contain 'You entered:'?
    step_settings = StepSettings(timeout=25000,
                                 sleep_time=1000,
                                 sleep_timing_type=SleepTimingType.Before,
                                 screenshot_condition=TakeScreenshotConditionType.Always)
    with DriverStepSettings(driver, step_settings):
        you_successfully_clicked_an_alert = driver.find_element(By.CSS_SELECTOR,
                                                                "#result")
        step_output = you_successfully_clicked_an_alert.text
        assert step_output and ("You entered:" in step_output)
