from .actions import ClickAndHold, ContextClick, DoubleClick, ExecuteJavascript, KeyDown, KeyUp, MoveMouseByOffset, ReleaseClick, AcceptAlert, DismissAlert, GetAlertText, SendKeysToAlert, WaitForAlertPresence, AddCookie, DeleteAllCookies, DeleteCookieByName, GetCookieByName, SaveAllCookiesToFile, ContainsAnyText, ContextClickOnElement, GetCharactersOnly, GetCssValue, GetElementAttribute, GetNumbersOnly, IsDisabled, IsEnabled, MoveMouseToElementByOffset, CheckIfSelectAllowsMultipleSelection, DeselectAllOptions, DeselectOptionByIndex, DeselectOptionsByValue, SelectOptionByIndex, SelectOptionByValue, SelectOptionbyVisibleText, SetElementAttribute, ClearLocalStorage, GetItemFromLocalStorage, RemoveItemFromLocalStorage, SetItemInLocalStorage, GetCurrentWindowHandle, GetWindowsList, MaximizeWindow, SetWindowPosition, SetWindowSize


class WebExtensions:
    @staticmethod
    def clickandhold() -> ClickAndHold:
        """Click and hold.

        Clicks and holds the mouse button at the last known mouse coordinates.
        """
        return ClickAndHold()

    @staticmethod
    def contextclick() -> ContextClick:
        """Context click.

        Right-clicks the mouse at the last known mouse coordinates.
        """
        return ContextClick()

    @staticmethod
    def doubleclick() -> DoubleClick:
        """Double click.

        Double-clicks the mouse at the last known mouse coordinates.
        """
        return DoubleClick()

    @staticmethod
    def executejavascript(code: str, args: str) -> ExecuteJavascript:
        """Execute JavaScript {{code}} with arguments {{args}}.

        Executes a JavaScript code.
        """
        return ExecuteJavascript(code, args)

    @staticmethod
    def keydown(theKey: str) -> KeyDown:
        """Press and hold {{theKey}}.

        Performs a modifier key press (kept pressed).
        """
        return KeyDown(theKey)

    @staticmethod
    def keyup(theKey: str) -> KeyUp:
        """Release {{theKey}}.

        Releases the key (shift, ctrl, etc) that was pressed.
        """
        return KeyUp(theKey)

    @staticmethod
    def movemousebyoffset(xOffset: int, yOffset: int) -> MoveMouseByOffset:
        """Moves mouse pointer by offset ({{xOffset}},{{yOffset}}).

        Moves the mouse from current position by the given offset.
        """
        return MoveMouseByOffset(xOffset, yOffset)

    @staticmethod
    def releaseclick() -> ReleaseClick:
        """Release left mouse button.

        Releases the mouse button at the last known mouse coordinates.
        """
        return ReleaseClick()

    @staticmethod
    def acceptalert(pause: int) -> AcceptAlert:
        """Accept alert.

        Accepts an alert by clicking on the 'OK' button of the alert.
        """
        return AcceptAlert(pause)

    @staticmethod
    def dismissalert(pause: int) -> DismissAlert:
        """Dismiss alert.

        Dismisses an alert by clicking on the 'Cancel' button of the alert.
        """
        return DismissAlert(pause)

    @staticmethod
    def getalerttext() -> GetAlertText:
        """Get alert text.

        Return to text parameter the text in the alert.
        """
        return GetAlertText()

    @staticmethod
    def sendkeystoalert(keys: str) -> SendKeysToAlert:
        """Send {{keys}} to alert.

        Sends keys (text) to an alert.
        """
        return SendKeysToAlert(keys)

    @staticmethod
    def waitforalertpresence(timeout: int) -> WaitForAlertPresence:
        """Is alert displayed?.

        Wait until the alert is displayed (default timeout is 0).
        """
        return WaitForAlertPresence(timeout)

    @staticmethod
    def addcookie(name: str, value: str, domain: str, path: str, expiry: str, secure: bool, httpOnly: bool) -> AddCookie:
        """Add cookie {{name}}.

        Adds a Cookie.
        """
        return AddCookie(name, value, domain, path, expiry, secure, httpOnly)

    @staticmethod
    def deleteallcookies() -> DeleteAllCookies:
        """Delete all cookies.

        Delete all the cookies for the current domain.
        """
        return DeleteAllCookies()

    @staticmethod
    def deletecookiebyname(name: str) -> DeleteCookieByName:
        """Delete a cookie with {{name}}.

        Delete the named cookie from the current domain.
        """
        return DeleteCookieByName(name)

    @staticmethod
    def getcookiebyname(name: str) -> GetCookieByName:
        """Get cookie {{name}}.

        Return specific cookie according to name.
        """
        return GetCookieByName(name)

    @staticmethod
    def saveallcookiestofile(folderPath: str, fileName: str) -> SaveAllCookiesToFile:
        """Saves all current session cookies to a text file."""
        return SaveAllCookiesToFile(folderPath, fileName)

    @staticmethod
    def containsanytext() -> ContainsAnyText:
        """{{element}} contains any text ?."""
        return ContainsAnyText()

    @staticmethod
    def contextclickonelement() -> ContextClickOnElement:
        """Context click {{element}}.

        Performs a context click at the current mouse position.
        """
        return ContextClickOnElement()

    @staticmethod
    def getcharactersonly() -> GetCharactersOnly:
        """Return only characters from the text."""
        return GetCharactersOnly()

    @staticmethod
    def getcssvalue(propertyName: str) -> GetCssValue:
        """Get value of {{propertyName}} CSS property on {{element}}.

        Gets CSS value from an element.
        """
        return GetCssValue(propertyName)

    @staticmethod
    def getelementattribute(attributeName: str) -> GetElementAttribute:
        """Get {{attributeName}} attribute value on {{element}}.

        Retrieves the value of an attribute of an element.
        """
        return GetElementAttribute(attributeName)

    @staticmethod
    def getnumbersonly() -> GetNumbersOnly:
        """Return only numbers from the text."""
        return GetNumbersOnly()

    @staticmethod
    def isdisabled() -> IsDisabled:
        """Run Is Disabled?."""
        return IsDisabled()

    @staticmethod
    def isenabled() -> IsEnabled:
        """Run Is Enabled?."""
        return IsEnabled()

    @staticmethod
    def movemousetoelementbyoffset(xOffset: int, yOffset: int) -> MoveMouseToElementByOffset:
        """Move mouse to {{element}} with offset ({{xOffset}},{{yOffset}}).

        Moves the mouse to an offset from the top-left corner of the element, element is scrolled into view.
        """
        return MoveMouseToElementByOffset(xOffset, yOffset)

    @staticmethod
    def checkifselectallowsmultipleselection() -> CheckIfSelectAllowsMultipleSelection:
        """Does {{element}} allows multiple selection?.

        Checks whether this select element support selecting multiple options at the same time.
        """
        return CheckIfSelectAllowsMultipleSelection()

    @staticmethod
    def deselectalloptions() -> DeselectAllOptions:
        """Deselect all options in {{element}}.

        Clear all selected entries.
        """
        return DeselectAllOptions()

    @staticmethod
    def deselectoptionbyindex(index: int) -> DeselectOptionByIndex:
        """Deselect an option indexed {{index}} in {{element}}.

        Deselect an option by it's given index (starts from 0).
        """
        return DeselectOptionByIndex(index)

    @staticmethod
    def deselectoptionsbyvalue(value: str) -> DeselectOptionsByValue:
        """Deselect all {{value}} options in {{element}}.

        Deselect all option with specified value attribute.
        """
        return DeselectOptionsByValue(value)

    @staticmethod
    def selectoptionbyindex(index: int) -> SelectOptionByIndex:
        """Select option indexed {{index}} in {{element}}.

        Deselect an option by it's given index (start from 0).
        """
        return SelectOptionByIndex(index)

    @staticmethod
    def selectoptionbyvalue(value: str) -> SelectOptionByValue:
        """Select all {{value}} options in {{element}}.

        Select all options with selected value attribute.
        """
        return SelectOptionByValue(value)

    @staticmethod
    def selectoptionbyvisibletext(value: str) -> SelectOptionbyVisibleText:
        """Select options in {{element}} with text {{text}}.

        Select any option with text that have a value matching the value parameter.
        """
        return SelectOptionbyVisibleText(value)

    @staticmethod
    def setelementattribute(attributeName: str, attributeValue: str) -> SetElementAttribute:
        """Set {{attributeName}} attribute value on {{element}}.

        Sets the value of an attribute of an element.
        """
        return SetElementAttribute(attributeName, attributeValue)

    @staticmethod
    def clearlocalstorage() -> ClearLocalStorage:
        """Clear local storage.

        Clears local storage.
        """
        return ClearLocalStorage()

    @staticmethod
    def getitemfromlocalstorage(key: str) -> GetItemFromLocalStorage:
        """Retrieve item {{key}} from local storage.

        Retrieves an item from local storage.
        """
        return GetItemFromLocalStorage(key)

    @staticmethod
    def removeitemfromlocalstorage(key: str) -> RemoveItemFromLocalStorage:
        """Removes item {{key}} from local storage.

        Removes an item from local storage.
        """
        return RemoveItemFromLocalStorage(key)

    @staticmethod
    def setiteminlocalstorage(key: str, value: str) -> SetItemInLocalStorage:
        """Set value {{value}} for {{key}} in local storage.

        Sets value for item in local storage.
        """
        return SetItemInLocalStorage(key, value)

    @staticmethod
    def getcurrentwindowhandle() -> GetCurrentWindowHandle:
        """Get current window handle.

        Return an opaque handle to this window that uniquely identifies it within this driver instance.
        """
        return GetCurrentWindowHandle()

    @staticmethod
    def getwindowslist() -> GetWindowsList:
        """Get windows handles.

        Return a set of window handles.
        """
        return GetWindowsList()

    @staticmethod
    def maximizewindow() -> MaximizeWindow:
        """Maximize window.

        Maximizes the current window.
        """
        return MaximizeWindow()

    @staticmethod
    def setwindowposition(x: str, y: str) -> SetWindowPosition:
        """Set window position to ({{x}},{{y}}).

        Set the position of the current window relative to the upper left corner of the screen.
        """
        return SetWindowPosition(x, y)

    @staticmethod
    def setwindowsize(width: str, height: str) -> SetWindowSize:
        """Set window size to ({{width}},{{height}}).

        Set the size of the current window.
        """
        return SetWindowSize(width, height)
