"""This module contains the CheckIfSelectAllowsMultipleSelection proxy action class."""
from src.testproject.classes import ProxyDescriptor
from src.testproject.sdk.addons import ActionProxy


class CheckIfSelectAllowsMultipleSelection(ActionProxy):
    def __init__(self):
        super().__init__()
        self.proxydescriptor = ProxyDescriptor(
            guid="Xey4bVsJQEODIUN7ssaJMg",
            classname="io.testproject.addons.web.element.select.CheckIfSelectAllowsMultipleSelection"
        )
        self.multiple = None
