"""This module contains the DeleteCookieByName proxy action class."""
from src.testproject.classes import ProxyDescriptor
from src.testproject.sdk.addons import ActionProxy


class DeleteCookieByName(ActionProxy):
    def __init__(self, name: str):
        super().__init__()
        self.proxydescriptor = ProxyDescriptor(
            guid="Xey4bVsJQEODIUN7ssaJMg",
            classname="io.testproject.addons.web.cookies.DeleteCookieByName"
        )
        self.name = name
