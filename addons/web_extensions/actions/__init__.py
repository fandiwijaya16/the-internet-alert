"""This package contains all the addon proxy's actions."""
from .clickandhold import ClickAndHold
from .contextclick import ContextClick
from .doubleclick import DoubleClick
from .executejavascript import ExecuteJavascript
from .keydown import KeyDown
from .keyup import KeyUp
from .movemousebyoffset import MoveMouseByOffset
from .releaseclick import ReleaseClick
from .acceptalert import AcceptAlert
from .dismissalert import DismissAlert
from .getalerttext import GetAlertText
from .sendkeystoalert import SendKeysToAlert
from .waitforalertpresence import WaitForAlertPresence
from .addcookie import AddCookie
from .deleteallcookies import DeleteAllCookies
from .deletecookiebyname import DeleteCookieByName
from .getcookiebyname import GetCookieByName
from .saveallcookiestofile import SaveAllCookiesToFile
from .containsanytext import ContainsAnyText
from .contextclickonelement import ContextClickOnElement
from .getcharactersonly import GetCharactersOnly
from .getcssvalue import GetCssValue
from .getelementattribute import GetElementAttribute
from .getnumbersonly import GetNumbersOnly
from .isdisabled import IsDisabled
from .isenabled import IsEnabled
from .movemousetoelementbyoffset import MoveMouseToElementByOffset
from .checkifselectallowsmultipleselection import CheckIfSelectAllowsMultipleSelection
from .deselectalloptions import DeselectAllOptions
from .deselectoptionbyindex import DeselectOptionByIndex
from .deselectoptionsbyvalue import DeselectOptionsByValue
from .selectoptionbyindex import SelectOptionByIndex
from .selectoptionbyvalue import SelectOptionByValue
from .selectoptionbyvisibletext import SelectOptionbyVisibleText
from .setelementattribute import SetElementAttribute
from .clearlocalstorage import ClearLocalStorage
from .getitemfromlocalstorage import GetItemFromLocalStorage
from .removeitemfromlocalstorage import RemoveItemFromLocalStorage
from .setiteminlocalstorage import SetItemInLocalStorage
from .getcurrentwindowhandle import GetCurrentWindowHandle
from .getwindowslist import GetWindowsList
from .maximizewindow import MaximizeWindow
from .setwindowposition import SetWindowPosition
from .setwindowsize import SetWindowSize

__all__ = ["ClickAndHold", "ContextClick", "DoubleClick", "ExecuteJavascript", "KeyDown", "KeyUp", "MoveMouseByOffset", "ReleaseClick", "AcceptAlert", "DismissAlert", "GetAlertText", "SendKeysToAlert", "WaitForAlertPresence", "AddCookie", "DeleteAllCookies", "DeleteCookieByName", "GetCookieByName", "SaveAllCookiesToFile", "ContainsAnyText", "ContextClickOnElement", "GetCharactersOnly", "GetCssValue", "GetElementAttribute", "GetNumbersOnly", "IsDisabled",
           "IsEnabled", "MoveMouseToElementByOffset", "CheckIfSelectAllowsMultipleSelection", "DeselectAllOptions", "DeselectOptionByIndex", "DeselectOptionsByValue", "SelectOptionByIndex", "SelectOptionByValue", "SelectOptionbyVisibleText", "SetElementAttribute", "ClearLocalStorage", "GetItemFromLocalStorage", "RemoveItemFromLocalStorage", "SetItemInLocalStorage", "GetCurrentWindowHandle", "GetWindowsList", "MaximizeWindow", "SetWindowPosition", "SetWindowSize"]
