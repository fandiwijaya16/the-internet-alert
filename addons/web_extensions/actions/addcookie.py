"""This module contains the AddCookie proxy action class."""
from src.testproject.classes import ProxyDescriptor
from src.testproject.sdk.addons import ActionProxy


class AddCookie(ActionProxy):
    def __init__(self, name: str, value: str, domain: str, path: str, expiry: str, secure: bool, httpOnly: bool):
        super().__init__()
        self.proxydescriptor = ProxyDescriptor(
            guid="Xey4bVsJQEODIUN7ssaJMg",
            classname="io.testproject.addons.web.cookies.AddCookie"
        )
        self.name = name
        self.value = value
        self.domain = domain
        self.path = path
        self.expiry = expiry
        self.secure = secure
        self.httpOnly = httpOnly
