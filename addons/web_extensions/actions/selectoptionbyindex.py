"""This module contains the SelectOptionByIndex proxy action class."""
from src.testproject.classes import ProxyDescriptor
from src.testproject.sdk.addons import ActionProxy


class SelectOptionByIndex(ActionProxy):
    def __init__(self, index: int):
        super().__init__()
        self.proxydescriptor = ProxyDescriptor(
            guid="Xey4bVsJQEODIUN7ssaJMg",
            classname="io.testproject.addons.web.element.select.SelectOptionByIndex"
        )
        self.index = index
