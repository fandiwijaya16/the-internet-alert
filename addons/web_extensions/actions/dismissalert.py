"""This module contains the DismissAlert proxy action class."""
from src.testproject.classes import ProxyDescriptor
from src.testproject.sdk.addons import ActionProxy


class DismissAlert(ActionProxy):
    def __init__(self, pause: int):
        super().__init__()
        self.proxydescriptor = ProxyDescriptor(
            guid="Xey4bVsJQEODIUN7ssaJMg",
            classname="io.testproject.addons.web.alert.DismissAlert"
        )
        self.pause = pause
