"""This module contains the GetElementAttribute proxy action class."""
from src.testproject.classes import ProxyDescriptor
from src.testproject.sdk.addons import ActionProxy


class GetElementAttribute(ActionProxy):
    def __init__(self, attributeName: str):
        super().__init__()
        self.proxydescriptor = ProxyDescriptor(
            guid="Xey4bVsJQEODIUN7ssaJMg",
            classname="io.testproject.addons.web.element.GetElementAttribute"
        )
        self.attributeName = attributeName
        self.attributeValue = None
